window.addEventListener('DOMContentLoaded', async () => {
  function makeError() {
    return `
      <div class="alert alert-danger" role="alert">
        Oops! Looks like something broke, please try again.
      </div>
    `;
  }
  
  function createCard(
    name,
    description,
    pictureUrl,
    formattedStarts,
    formattedEnds,
    location
  ) {
    return `
        <div class="card shadow mb-5 bg-white rounded" style="height: min-content;">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
          ${formattedStarts} - ${formattedEnds}
        </div>
        </div>
      `;
  }
  

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = details.conference.starts;
          const dateStarts = new Date(starts);
          const formattedStarts = `${dateStarts.getMonth()+ 1}/${dateStarts.getDate()}/${dateStarts.getFullYear()}`;
          const ends = details.conference.ends;
          const dateEnds = new Date(ends);
          const formattedEnds = `${dateEnds.getMonth() + 1}/${dateEnds.getDate()}/${dateEnds.getFullYear()}`;
          const location = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, formattedStarts, formattedEnds, location);
          const column = document.querySelector('.col');
          column.innerHTML += html;
        }
      }

    }
  } catch (error) {
    // Figure out what to do if an error is raised
    const html = generateError();
    const column = document.querySelector('.row');
    column.innerHTML += html;

  }

});
