if(response.ok){
    const data= await response.json();
    console.log(data)

    const selectLocation = document.getElementById('location');
    for (let locate of data.locations){
        const option = document.createElement("option")
        option.value = locate.id;
        option.innerHTML = locate.name;
        selectLocation.append(option)
    }
    
}