import React from 'react';

class PresentationForm extends React.Component {
    // constructor
    constructor(props){
        super(props);
        // set starting/default state - empties
        this.state = {
            company_name: '',
            presenter_name: '',
            presenter_email: '',
            title: '',
            synopsis: '',
            conferences: [],
        };

        // add in all this handlers
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCompanyChange = this.handleCompanyChange.bind(this);
        this.handleConferenceChange = this.handleConferenceChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
    }

    //componentDidMount
    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({conferences: data.conferences});
        }
    }


    //handle submit
    // put other file js here
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};       
        console.log(data)
    
        const conferenceUrl = `http://localhost:8000${this.state.conference}presentations/`;
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };
        const response = await fetch(conferenceUrl, fetchConfig);             
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference)
            
            const cleared = {
                company_name: '',
                presenter_name: '',
                presenter_email: '',
                title: '',
                synopsis: '',
                conferences: [], 
              };
            this.setState(cleared);
        }
    }


    //all handlers
    handleCompanyChange(event) {
        const value = event.target.value;
        this.setState({ company_name: value });
      }
    
      handleNameChange(event) {
        const value = event.target.value;
        this.setState({ presenter_name: value });
      }
    
      handleEmailChange(event) {
        const value = event.target.value;
        this.setState({ presenter_email: value });
      }
    
      handleTitleChange(event) {
        const value = event.target.value;
        this.setState({ title: value });
      }
    
      handleSynopsisChange(event) {
        const value = event.target.value;
        this.setState({ synopsis: value });
      }
    
      handleConferenceChange(event) {
        const value = event.target.value;
        this.setState({ conference: value });
      }

    // render - return html from other file
    render() {
        return(
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={this.handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                            <label for="presenter_name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                            <label for="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleCompanyChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" />
                            <label for="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                            <label for="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label onChange={this.handleSynopsisChange} for="synopsis">Synopsis</label>
                            <textarea className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleConferenceChange} required name="conference" id="conference" className="form-select" >
                            <option selected value="">Choose a conference</option>
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        );
    }

};

export default PresentationForm;